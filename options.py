#!/usr/bin/python3

# Copyright (C) 2018-2019 Ralf Treinen <treinen@debian.org>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

import argparse

argparser=argparse.ArgumentParser(
  description="Run dose-debcheck and aggregate the results.")
argparser.add_argument(
    '--config',
    dest='configfile',
    action='store',
    required=False,
    default='conf',
    help='configuration file, with or without final .py (default: conf)'
)
argparser.add_argument(
    '--dump-bugtable',
    dest='dump_bugtable',
    action='store_true',
    default=False,
    help='dump contents of bugtable directly after retrival from the BTS (default: False)'
)
arguments=argparser.parse_args()
if len(arguments.configfile) > 3 and arguments.configfile[-3:] == '.py':
    confmodule = arguments.configfile[:-3]
else:
    confmodule = arguments.configfile 
dump_bugtable=arguments.dump_bugtable
